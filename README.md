# OpenFaaS Minikube

## Synopsis
This repository holds several Ansible roles to deploy OpenFaas on top of Minikube within a Debian 10 VM.

## Roles
Here below for each role is provided a brief description and the available variables.

### base
The base role updates the APT cache and upgrade all the packages. Then, it installs base packages and utilities.

### kubectl
The kubectl role installs kubectl from the official APT repository along with the bash completion.

### minikube
The minikube role installs `libvirt` related packages since the Minikube VM is provisioned by KVM.
Then, it installs minikube and runs a VM with the KVM2 driver.
Finally, it manages iptables rules to handle external traffic to the Kubernetes API.

**Variables**

| Name | Description | Default value |
|------------------------------|-------------------------------------------------------------------|---------------|
| public_ip | The public IP used to generate Kubernetes API valid certificates | 127.0.0.1 |
| minikube_version | The Minikube version | 1.4.0 |
| minikube_cpus | The number of vCPUs of the Minikube VM | 2 |
| minikube_memory | The memory of the Minikube VM | 4gb |
| minikube_disk_size | The disk size of the Minikube VM | 40gb |
| minikube_kubernetes_api_port | The Kubernetes API port | 8443 |
| public_kubernetes_api_port | The public Kubernetes API port used for external incoming traffic | 8443 |

### openfaas
The openfaas role installs the faas-cli, deploys OpenFaas (faas-netes), exposes Prometheus with a NodePort Service, makes the login with the CLI and also it manages iptables rules to handle external traffic to the OpenFaas Gateway and to Prometheus.

| Name | Description | Default value |
|-----------------------------------|------------------------------------------------------------------------|---------------|
| public_openfaas_gateway_port | The public OpenFaas Gateway port used for external incoming traffic | 80 |
| public_openfaas_prometheus_port | The public OpenFaas Prometheus port used for external incoming traffic | 9090 |

### kube-state-metrics
The kube-state-metrics role deploys kube-state-metrics within the `openfaas` Kubernetes Namespace.

| Name | Description | Default value |
|-----------------------------------|------------------------------------------------------------------------|---------------|
| kube_state_metrics_version | The kube-state-metrics version | v1.8.0 |

## License
This project is licensed under the GPLv3. See the [LICENSE.md](LICENSE.md) file for details.
